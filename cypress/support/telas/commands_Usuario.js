Cypress.Commands.add('AddUsuario', () => {
    const Leite = require('leite');
    const leite = new Leite();

    //Click Botão Add
    cy.get('.btn-add').should('have.text', '+ Usuário').click();
    //Title
    cy.get('.alert-header-title').should('have.text', 'Criar usuário');
    cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
    cy.get('.btn-add').should('have.text', '+ Usuário').click();
    //Title
    cy.get('.alert-header-title').should('have.text', 'Criar usuário');
    //campo nome usuario
    const nomeUsuario = `${leite.pessoa.primeiroNome()} ${leite.pessoa.sobrenome()}`;
    cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
        const text = $element.text();
        expect(text).to.include('Nome*');
    }).type(nomeUsuario);
    //campo telefone
    const telefoneUsuario = "41" + Math.floor(Math.random() * 100000) + Math.floor(Math.random() * 10000);
    cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
        const text = $element.text();
        expect(text).to.include('Telefone*');
    }).type(telefoneUsuario);
    //campo data nascimento
    const dtaNascimentoUsuario = leite.pessoa.nascimento({ formato: 'YYYY-MM-DD' });
    cy.get(':nth-child(3) > .form > .label-input').should(($element) => {
        const text = $element.text();
        expect(text).to.include('Data de Nascimento');
    }).type(dtaNascimentoUsuario);
    //campo email
    const emailUsuario = leite.pessoa.email();
    cy.get(':nth-child(4) > .form > .label-input').should(($element) => {
        const text = $element.text();
        expect(text).to.include('E-mail*');
    }).type(emailUsuario);
    //Select tipo de usuario
    cy.get(':nth-child(5) > .form > .label-input').should(($element) => {
        const text = $element.text();
        expect(text).to.include('Selecione um tipo de usuário');
    });
    //Campos adicionais de aluno, nao devem aparecer
    cy.get('.additional-fields > :nth-child(1) > .form > .label-input').should('not.exist');
    cy.get('.additional-fields > :nth-child(2) > .form > .label-input').should('not.exist');
});