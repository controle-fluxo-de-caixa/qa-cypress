Cypress.Commands.add('login_sucesso_static', () => {
    cy.visit("/");
    cy.fixture('login_dados.json').then((data) => {
        cy.fixture('login_campos.json').then((fields) => {
            cy.get(fields["input_email"]).type(data["email_sucesso"])
            cy.get(fields["input_senha"]).type(data["senha_sucesso"])
        });
    });

    cy.get('.btn').contains('Login').click()
    cy.url().should("eq", `${Cypress.config(`baseUrl`)}/perfil`);
});


Cypress.Commands.add('login_sucesso_params', ({ usuario, senha }) => {
    cy.visit("/");
    cy.fixture('login_campos.json').then((fields) => {
        cy.get(fields["input_email"]).type(usuario)
        cy.get(fields["input_senha"]).type(senha)

    });

    cy.get('.btn').contains('Login').click()
    cy.url().should("eq", `${Cypress.config(`baseUrl`)}/perfil`);
});
