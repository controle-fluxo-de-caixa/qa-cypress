import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";

When("preencho o campo {string} com {string}", (campo, valor) => {
    cy.fixture('login_campos.json').then((data) => {
        cy.get(data[campo]).type(valor)
    })
});