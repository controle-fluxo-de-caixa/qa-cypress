import { expect } from "chai";
import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";

When("clico no botao {string}", (buttonText) => {
  cy.get('.password-login').contains(buttonText).click()
});


And("sou redirecionado para a tela de recuperacao de senha", () => {
  cy.url().should("eq", `${Cypress.config(`baseUrl`)}/esqueci_senha`);
});


When("clica no botão da tela Esqueci Minha Senha {string}", (buttonText) => {
  cy.contains(buttonText).click();
});

Then("aparece um alerto com o texto Verifique seu email", () => {
  cy.wait(500)
  cy.intercept("POST", "http://localhost:3000/forgotPassword/sendMail", (req) => {
    expect(req.body).to.deep.equal({
      email: "inspirarbalanco@gmail.com"
    });
    expect(req.reply.statusCode).eq(200)
  });

  cy.on('window:alert', (alertText) => {
    cy.log('Alert text:', alertText)
    expect(alertText).have.to.string("Verifique seu email para continuar a recuperação de senha")
    cy.on('window:alert', () => true) // to confirm
  })
});


Then("sou redirecionado para a tela de login", () => {

});
