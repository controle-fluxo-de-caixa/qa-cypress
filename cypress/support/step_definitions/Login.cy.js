import { Given, When, And, Then } from "cypress-cucumber-preprocessor/steps";

Given("que estou na página de login", () => {
    cy.visit("/");
});

And("clico no botão {string}", (botao) => {
    cy.get('.btn').contains(botao).click()
});

Then("devo ser redirecionado para a página inicial", () => {
    cy.url().should("eq", `${Cypress.config(`baseUrl`)}/perfil`);
});

Then("devo ver uma mensagem de erro indicando credenciais invalidas", () => {
    cy.get('.error-message').contains('E-mail ou senha incorretos. Por favor, tente novamente')
});