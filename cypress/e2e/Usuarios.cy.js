///<reference types="cypress"/>

describe('Usuários', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', '/accounts/users').as('GetUsuariosRequest');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Usuários
        cy.get(':nth-child(5) > .menu-item-text').should('have.text', 'Usuários').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/criar_usuario`);

        cy.wait('@GetUsuariosRequest').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });
    });
    it('Validar componentes da tela', () => {
        //Título da página
        cy.get('.page-title').should('have.text', 'Usuários');
        //Campos table
        cy.get('thead > tr > :nth-child(1)').should('have.text', 'ID');
        cy.get('thead > tr > :nth-child(2)').should('have.text', 'Nome');
        cy.get('thead > tr > :nth-child(3)').should('have.text', 'Telefone');
        cy.get('thead > tr > :nth-child(4)').should('have.text', 'Data de Nascimento');
        cy.get('thead > tr > :nth-child(5)').should('have.text', 'E-mail');
        cy.get('thead > tr > :nth-child(6)').should('have.text', 'Tipo de Usuário');
        cy.get('thead > tr > :nth-child(7)').should('have.text', 'Ações');
        //Botão add
        cy.get('.btn-add').should('have.text', '+ Usuário');
    });
    it('Adicionar Usuário (ADMINISTRADOR)', () => {
        cy.intercept('POST', '/accounts/user').as('criarUserRequest');
        //Função para preencher campos do AddUsuario
        cy.AddUsuario();
        //Select tipo de usuario
        cy.get(':nth-child(5) > .form > .label-input').select("Administrador");
        //botao cancelar
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');
        //botao salvar 
        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarUserRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Usuário criado com sucesso.');
            expect(json.response.body.status).to.eq(201);
            expect(json.response.body.model).to.exist

            const emailLogin = json.request.body.email;
            const senhaLogin = json.response.body.model;
            cy.log(emailLogin, senhaLogin)
            cy.login_sucesso_params({ "usuario": emailLogin, "senha": senhaLogin });
        });
    });
    it('Adicionar Usuário (FUNCIONÁRIO)', () => {
        cy.intercept('POST', '/accounts/user').as('criarUserRequest');
        //Função para preencher campos do AddUsuario
        cy.AddUsuario();
        //Select tipo de usuario
        cy.get(':nth-child(5) > .form > .label-input').select("Funcionário");
        //botao cancelar
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');
        //botao salvar 
        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarUserRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Usuário criado com sucesso.');
            expect(json.response.body.status).to.eq(201);
            expect(json.response.body.model).to.exist

            const emailLogin = json.request.body.email;
            const senhaLogin = json.response.body.model;
            cy.log(emailLogin, senhaLogin)
            cy.login_sucesso_params({ "usuario": emailLogin, "senha": senhaLogin });
        });
    });
    it('Adicionar Usuário (ALUNO)', () => {
        cy.intercept('POST', '/accounts/user').as('criarUserRequest');
        //Função para preencher campos do AddUsuario
        cy.AddUsuario();
        //Select tipo de usuario
        cy.get(':nth-child(5) > .form > .label-input').select("Aluno");
        //Campos adicionais
        let tipoAulaUsuario;
        cy.get('.additional-fields > :nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            const arrayText = text.split('*');
            expect(arrayText[0]).to.include('Selecione uma aula para matricular');
            tipoAulaUsuario = arrayText[1];
        });
        //Selecionar aula
        cy.get('.additional-fields > :nth-child(1) > .form > .label-input').find('option:eq(1)').invoke('val').then((value) => {
            cy.get('.additional-fields > :nth-child(1) > .form > .label-input').select(value);
        });
        //Nome Responsavel
        const nomeResponsavelUsuario = `${leite.pessoa.primeiroNome()} ${leite.pessoa.sobrenome()}`;
        cy.get('.additional-fields > :nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Nome do responsável*');
        }).type(nomeResponsavelUsuario);
        //botao cancelar
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');
        //botao salvar 
        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarUserRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Usuário criado com sucesso.');
            expect(json.response.body.status).to.eq(201);
            expect(json.response.body.model).to.exist

            const emailLogin = json.request.body.email;
            const senhaLogin = json.response.body.model;
            cy.log(emailLogin, senhaLogin)
            cy.login_sucesso_params({ "usuario": emailLogin, "senha": senhaLogin });
        });
    });
    it('Excluir Usuário', () => {
        cy.intercept('DELETE', '/accounts/user').as('excluirUserRequest');
        //Botao Excluir
        cy.get(':nth-child(1) > :nth-child(7) > .material-icons').click();
        //Modal confirmar exclusão
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir o usuário?');
        cy.get('.btn-cancel').should('have.text', 'Cancelar').click();
        //Exclusão de volta
        cy.get(':nth-child(1) > :nth-child(7) > .material-icons').click();
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir o usuário?');
        cy.get('.btn-confirm').should('have.text', 'Confirmar').click();

        cy.wait('@excluirUserRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Usuário excluído com sucesso');
            expect(json.response.body.status).to.eq(200);
        });
    });
});