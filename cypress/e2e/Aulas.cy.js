///<reference types="cypress"/>

describe('Aulas', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', '/class/all').as('GetAulaRequest');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Aulas
        cy.get(':nth-child(4) > .menu-item-text').should('have.text', 'Aulas').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/aulas`);

        cy.wait('@GetAulaRequest').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });
        
    });
    it('Validar componentes da tela', () => {
        //Título da página
        cy.get('.page-title').should('have.text', 'Aulas');
        //Campos table
        cy.get('thead > tr > :nth-child(1)').should('have.text', 'Nome');
        cy.get('thead > tr > :nth-child(2)').should('have.text', 'Valor');
        cy.get('thead > tr > :nth-child(3)').should('have.text', 'Alunos');
        cy.get('thead > tr > :nth-child(4)').should('have.text', 'Ações');
        //Botão add
        cy.get('.btn-add').should('have.text', '+ Aula');
    });
    it('Adicionar Aula', () => {
        cy.intercept('POST', '/class/').as('criarAulaRequest');
        //Click Botão Add
        cy.get('.btn-add').should('have.text', '+ Aula').click();
        //Title
        cy.get('.alert-header-title').should('have.text', 'Adicionar aula');
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('.btn-add').should('have.text', '+ Aula').click();
        //Title
        cy.get('.alert-header-title').should('have.text', 'Adicionar aula');
        //Form
        const nomeAula = `Ginastica - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Nome da Aula*');
        }).type(nomeAula);
        const valorAula = (Math.random() * 100).toFixed(2);
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Valor*');
        }).type(valorAula);

        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');
        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarAulaRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Classe inserida com sucesso.');
            expect(json.response.body.status).to.eq(201);
        });
    });
    it('Excluir Aula', () => {
        cy.intercept('DELETE', '/class/*').as('ExcluirAulaRequest');
        cy.get('tbody > :nth-child(1) > :nth-child(4) > :nth-child(2)').click();
        //Modal confirmar exclusão
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a Aula?');
        cy.get('.btn-cancel').should('have.text', 'Cancelar').click();
        //Exclusão de volta
        cy.get('tbody > :nth-child(1) > :nth-child(4) > :nth-child(2)').click();
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a Aula?');
        cy.get('.btn-confirm').should('have.text', 'Confirmar').click();
        
        cy.wait('@ExcluirAulaRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Classe deletada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });
    it('Editar Aulas', () => {
        cy.intercept('PATCH', '/class/*').as('EditarAulaRequest');

        cy.get('tbody > :nth-child(1) > :nth-child(4) > :nth-child(1)').click();
        //Titulo do card
        cy.get('.alert-header-title').should('have.text', 'Editar aula');
        //Botao Cancelar
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        //Abrir card novamente
        cy.get('tbody > :nth-child(1) > :nth-child(4) > :nth-child(1)').click();
        //Titulo do card
        cy.get('.alert-header-title').should('have.text', 'Editar aula');

        const nomeAula = `Dança - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').then((element) => {
            cy.log('Selected Element:', element);
        });

        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Nome da Aula*');
        }).type("teste")
        cy.get(':nth-child(1) > .form > .label-input').clear().type(nomeAula);

        const valorAula = (Math.random() * 100).toFixed(2);
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Valor*');
        }).clear().type(valorAula);

        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');
        cy.get('.is-primary').should('have.text', 'Atualizar').click();

        cy.wait('@EditarAulaRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Classe atualizada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });
});