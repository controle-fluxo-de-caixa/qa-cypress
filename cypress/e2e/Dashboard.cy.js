///<reference types="cypress"/>

describe('Dashboard', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', '/financial/dashboard').as('GetDashboard');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Dashboard
        cy.get('.side-menu > :nth-child(2)').should('have.text', 'Meu Dashboard').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/dashboard`);
        cy.get('.page-title').should('have.text', 'Dashboard');

        cy.wait('@GetDashboard').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });

    });
    it('Atualizar Dashboard', () => {
        cy.intercept('PUT', '/financial/dashboard').as('PutDashboard');

        cy.get('.btn-upt').should('have.text', 'Atualizar Dados').click();
        cy.wait('@GetDashboard').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });
    });
});