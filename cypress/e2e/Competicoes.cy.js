///<reference types="cypress"/>
const Leite = require('leite');
const leite = new Leite();
const moment = require('moment');



describe('Competições', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', 'competition/').as('GetCompetition');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Notificacao
        cy.get(':nth-child(8) > .menu-item-text').should('have.text', 'Competições').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/competicoes`);

        cy.wait('@GetCompetition').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });

    });
    it('Validar componentes da tela', () => {
        //Título da página
        cy.get('.page-title').should('have.text', 'Competições');
        //Campos table
        cy.get('thead > tr > :nth-child(1)').should('have.text', 'Nome');
        cy.get('thead > tr > :nth-child(2)').should('have.text', 'Data');
        cy.get('thead > tr > :nth-child(3)').should('have.text', 'Localização');
        cy.get('thead > tr > :nth-child(4)').should('have.text', 'Participantes');
        cy.get('thead > tr > :nth-child(5)').should('have.text', 'Ações');
    });

    it('Criar uma Competição', () => {
        cy.intercept('POST', '/competition/').as('criarCompeticaoRequest');

        cy.get('.btn-add').should('have.text', '+ Competição').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Adicionar Competição");
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('.btn-add').should('have.text', '+ Competição').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Adicionar Competição");

        const nomeCompeticao = `Competição - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Nome');
        }).type(nomeCompeticao);

        const dataAtual = moment().format("YYYY-MM-DD");
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Data');
        }).type(dataAtual);

        const localizacaoCompeticao = `${leite.localizacao.logradouro()} ${leite.localizacao.complemento()} ${leite.localizacao.cidade()}`
        cy.get(':nth-child(3) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Localização');
        }).type(localizacaoCompeticao);

        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarCompeticaoRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Competição criada com Sucesso');
            expect(json.response.body.status).to.eq(201);
        });
    });

    it('Editar uma Competição', () => {
        cy.intercept('PATCH', '/competition/*').as('editarCompeticaoRequest');

        cy.get('tbody > :nth-child(1) > :nth-child(5) > :nth-child(1)').click();
        
        cy.get('.alert-header-title').should('have.text', "Editar Competição");
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('tbody > :nth-child(1) > :nth-child(5) > :nth-child(1)').click();
        
        cy.get('.alert-header-title').should('have.text', "Editar Competição");

        const nomeCompeticao = `Competição - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Nome');
        }).type(nomeCompeticao);
        cy.get(':nth-child(1) > .form > .label-input').clear().type(nomeCompeticao);

        const dataAtual = moment().format("YYYY-MM-DD");
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Data');
        }).type(dataAtual);
        cy.get(':nth-child(2) > .form > .label-input').clear().type(dataAtual);

        const localizacaoCompeticao = `${leite.localizacao.logradouro()} ${leite.localizacao.complemento()} ${leite.localizacao.cidade()}`
        cy.get(':nth-child(3) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Localização');
        }).type(localizacaoCompeticao);
        cy.get(':nth-child(3) > .form > .label-input').clear().type(localizacaoCompeticao);

        cy.get('.is-primary').should('have.text', 'Atualizar').click();

        cy.wait('@editarCompeticaoRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Competição atualizada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });

    it.only('Excluir uma Competição', () => {
        cy.intercept('DELETE', '/competition/*').as('ExcluirCompeticaoRequest');

        cy.get('tbody > :nth-child(1) > :nth-child(5) > :nth-child(2)').click();

        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a competição?');
        cy.get('.btn-cancel').should('have.text', 'Cancelar').click();
        //Exclusão de volta
        cy.get('tbody > :nth-child(1) > :nth-child(5) > :nth-child(2)').click();
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a competição?');
        cy.get('.btn-confirm').should('have.text', 'Confirmar').click();

        cy.wait('@ExcluirCompeticaoRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Competição deletada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });
});