///<reference types="cypress"/>

describe('Notificações', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', 'notification/*').as('GetNotification');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Notificacao
        cy.get(':nth-child(7) > .menu-item-text').should('have.text', 'Notificações').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/notificacoes`);

        cy.wait('@GetNotification').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });

    });
    it('Validar componentes da tela', () => {
        //Título da página
        cy.get('.page-title').should('have.text', 'Notificações');
        //Campos table
        cy.get('thead > tr > :nth-child(1)').should('have.text', 'Mensagem');
        cy.get('thead > tr > :nth-child(2)').should('have.text', 'Data');
        cy.get('thead > tr > :nth-child(3)').should('have.text', 'Ações');
    });
});