///<reference types="cypress"/>

describe('Movimentações Financeiras', () => {
    const Leite = require('leite');
    const leite = new Leite();
    const moment = require('moment');

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', '/financial/movements').as('GetMovimentacoesFinanceirasRequest');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Movimentações Financeiras
        cy.get(':nth-child(3) > .menu-item-text').should('have.text', 'Movimentações').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/movimentacao`);
        cy.wait('@GetMovimentacoesFinanceirasRequest').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });
    });
    it('Validar componentes da tela', () => {
        //Título da página
        cy.get('.page-title').should('have.text', 'Movimentação Financeira');
        //Campos table
        cy.get('thead > tr > :nth-child(1)').should('have.text', 'Data');
        cy.get('thead > tr > :nth-child(2)').should('have.text', 'Descrição');
        cy.get('thead > tr > :nth-child(3)').should('have.text', 'Valor');
        cy.get('thead > tr > :nth-child(4)').should('have.text', 'Categoria');
        cy.get('thead > tr > :nth-child(5)').should('have.text', 'Tipo de Movimentação');
        cy.get('thead > tr > :nth-child(6)').should('have.text', 'Ações');
        //Botão add
        cy.get('.btn-add').should('have.text', '+ Movimentação');
        //Blocos de valores
        cy.get('.spending > .box-2-header').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Saldo: R$');
        });
        cy.get(':nth-child(2) > .box-2-header').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Total de Gastos: R$');
        });
        cy.get(':nth-child(3) > .box-2-header').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Total de Ganhos: R$');
        });
    });
    it('Adicionar Movimentação Financeira', () => {
        cy.intercept('POST', '/financial/movement').as('criarMovimentacaoFinanceiraRequest');
        //Click Botão Add
        cy.get('.btn-add').should('have.text', '+ Movimentação').click();
        //Title
        cy.get('.alert-header-title').should('have.text', 'Adicionar movimentação');
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('.btn-add').should('have.text', '+ Movimentação').click();
        //Title
        cy.get('.alert-header-title').should('have.text', 'Adicionar movimentação');
        //Form
        const descricaoMovimentacaoFinanceira = `Compra de materiais - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Descrição');
        }).type(descricaoMovimentacaoFinanceira);

        const valorMovimentacaoFinanceira = (Math.random() * 100).toFixed(2);
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Valor');
        }).type(valorMovimentacaoFinanceira);

        const dataAtual = moment().format("YYYY-MM-DD");
        cy.get('[modelvalue=""] > .form > .label-input > .input-container > .input-text').type(dataAtual);

        cy.get('.alert-content > :nth-child(4)').select("Educação");
        cy.get('.alert-content > :nth-child(4)').select("Conta");
        cy.get('.alert-content > :nth-child(4)').select("Mensalidade");
        cy.get('.alert-content > :nth-child(4)').select("Material de Ginástica");

        cy.get('.alert-content > :nth-child(5)').select("Gasto");
        cy.get('.alert-content > :nth-child(5)').select("Ganho");

        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar');

        cy.get('.is-primary').should('have.text', 'Salvar').click();

        cy.wait('@criarMovimentacaoFinanceiraRequest').then((json) => {
            expect(json.response.statusCode).to.eq(201);
            expect(json.response.body.message).to.be.string('Movimentação financeira inserida com sucesso.');
            expect(json.response.body.status).to.eq(201);
        });
    });
    it('Excluir Movimentação Financeira', () => {
        cy.intercept('DELETE', '/financial/movement/*').as('ExcluirMovimentacaoFinanceiraRequest');
        //Botao Excluir
        cy.get('tbody > :nth-child(1) > :nth-child(6) > :nth-child(2)').click();
        //Modal confirmar exclusão
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a movimentação financeira?');
        cy.get('.btn-cancel').should('have.text', 'Cancelar').click();
        //Exclusão de volta
        cy.get('tbody > :nth-child(1) > :nth-child(6) > :nth-child(2)').click();
        cy.get('p').should('have.text', 'Tem certeza que deseja excluir a movimentação financeira?');
        cy.get('.btn-confirm').should('have.text', 'Confirmar').click();

        cy.wait('@ExcluirMovimentacaoFinanceiraRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Movimentação deletada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });
    it('Editar Movimentação Financeira', () => {
        cy.intercept('PATCH', '/financial/movement/*').as('EditarMovimentacaoFinanceiraRequest');
        //Botao Editar
        cy.get('tbody > :nth-child(1) > :nth-child(6) > :nth-child(1)').click();
        //Titulo do card
        cy.get('.alert-header-title').should('have.text', 'Editar movimentação');
        //Botao Cancelar
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('tbody > :nth-child(1) > :nth-child(6) > :nth-child(1)').click();
        //Titulo do card
        cy.get('.alert-header-title').should('have.text', 'Editar movimentação');

        const descricaoMovimentacaoFinanceira = ` - ${leite.localizacao.cep()}`;
        cy.get(':nth-child(1) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Descrição');
        }).type(descricaoMovimentacaoFinanceira);

        const valorMovimentacaoFinanceira = (Math.random() * 100).toFixed(2);
        cy.get(':nth-child(2) > .form > .label-input').should(($element) => {
            const text = $element.text();
            expect(text).to.include('Valor');
        }).clear().type(valorMovimentacaoFinanceira);

        const dataAtual = moment().format("YYYY-MM-DD");
        cy.get(':nth-child(3) > .form > .label-input > .input-container > .input-text').clear().type(dataAtual);

        cy.get('.is-primary').should('have.text', 'Atualizar').click();

        cy.wait('@EditarMovimentacaoFinanceiraRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Movimentação atualizada com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });

})
