///<reference types="cypress"/>

describe('Agenda', () => {
    const Leite = require('leite');
    const leite = new Leite();

    beforeEach(() => {
        cy.viewport(1920, 1080);
        cy.intercept('GET', '/reminder/get_all').as('GetReminder');
        //Login no sistema 
        cy.login_sucesso_static();
        //Entrar no menu de Agenda
        cy.get(':nth-child(9) > .menu-item-text').should('have.text', 'Agenda').click();
        cy.url().should("eq", `${Cypress.config(`baseUrl`)}/calendario`);

        cy.get('.page-title').should('have.text', 'Agenda');

        cy.wait('@GetReminder').then((json) => {
            expect(json.response.statusCode).to.satisfy((status) => status === 200 || status === 304);
        });
        
    });
    it('Criar um Lembrete', () => {
        cy.get('.btn-add').should('have.text', '+ Lembrete').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Adicionar Lembrete");
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get('.btn-add').should('have.text', '+ Lembrete').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Adicionar Lembrete");
    });

    it('Editar um Lembrete', () => {
        cy.get(':nth-child(1) > .card_reminder > .action-options > :nth-child(1)').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Editar Lembrete");
        cy.get('.alert-footer > :nth-child(1)').should('have.text', 'Cancelar').click();
        cy.get(':nth-child(1) > .card_reminder > .action-options > :nth-child(1)').click();
        //Title
        cy.get('.alert-header-title').should('have.text', "Editar Lembrete");
    });

    it('Excluir um Lembrete', () => {
        cy.intercept('DELETE', '/reminder/*').as('ExcluirReminderRequest');
        //Botao Excluir
        cy.get(':nth-child(1) > .card_reminder > .action-options > :nth-child(2)').click();
        //Modal confirmar exclusão
        cy.get('.caixa > p').should('have.text', 'Tem certeza que deseja excluir o lembrete?');
        cy.get('.btn-cancel').should('have.text', 'Cancelar').click();
        //Exclusão de volta
        cy.get(':nth-child(1) > .card_reminder > .action-options > :nth-child(2)').click();
        cy.get('.caixa > p').should('have.text', 'Tem certeza que deseja excluir o lembrete?');
        cy.get('.btn-confirm').should('have.text', 'Confirmar').click();

        cy.wait('@ExcluirReminderRequest').then((json) => {
            expect(json.response.statusCode).to.eq(200);
            expect(json.response.body.message).to.be.string('Lembrete deletado com sucesso.');
            expect(json.response.body.status).to.eq(200);
        });
    });
});