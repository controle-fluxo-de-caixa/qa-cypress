Feature: Recuperação de Senha

  Scenario: Recuperação de senha com email valido
    Given que estou na página de login
    When clico no botao "Esqueceu sua senha?"
    And sou redirecionado para a tela de recuperacao de senha
    And preencho o campo "btn_enviar_email" com "inspirarbalanco@gmail.com"
    And clica no botão da tela Esqueci Minha Senha "Enviar Email"
    Then aparece um alerto com o texto Verifique seu email
