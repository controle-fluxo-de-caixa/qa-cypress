Feature: Login de Usuário

  Scenario: Login de usuário com informações corretas
    Given que estou na página de login
    When preencho o campo "input_email" com "inspirarbalanco@gmail.com"
    And preencho o campo "input_senha" com "12345"
    And clico no botão "Login"
    Then devo ser redirecionado para a página inicial

  Scenario: Login de usuário com informações incorretas
    Given que estou na página de login
    When preencho o campo "input_email" com "email_incorreto@gmail.com"
    And preencho o campo "input_senha" com "senha_incorreta"
    And clico no botão "Login"
    Then devo ver uma mensagem de erro indicando credenciais invalidas
