const { defineConfig } = require("cypress");
const cucumber = require('cypress-cucumber-preprocessor').default;
require('dotenv').config()

module.exports = defineConfig({
  e2e: {
    retries: 0,
    specPattern: ["**/integration/*.feature", "**/e2e/*.cy.js"],
    baseUrl: process.env.BASE_URL,
    nonGlobalStepDefinitions: true,
    setupNodeEvents(on, config) {
      on('file:preprocessor', cucumber());
    },
  },
});
